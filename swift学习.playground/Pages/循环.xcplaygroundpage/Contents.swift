//: [Previous](@previous)

import Foundation
import UIKit

//: [Next](@next)
// 0..<10  是从 0 到 9
for i in 0 ..< 10 {
    print(i)
}
// 0...10 是从 0 到 10
for i in 0...10{
    print(i)
}
//Range<Int> 泛型
let range = 0...10
for i in range{
    print(i)
}