//: [Previous](@previous)

import Foundation

var str = "Hello, playground"

//: [Next](@next)

// 可以暂时理解为 oc 中的 Block
/*
 1.一组预先准备好的代码
 2.可以当参数传递啊
 3.在需要的时候执行
 
 
 
 */
func sum(num1 x: integer_t,num2 y: integer_t) ->integer_t{
    return x + y
}
//sum(x: 10, y: 20)
sum(num1: 10, num2: 30)
//在 Swift 中可以变量直接记录函数
let sumFunc  = sum
// 执行函数
sumFunc(10, 40)
let temp = { () -> () in
    print("please send message if you need")
}





