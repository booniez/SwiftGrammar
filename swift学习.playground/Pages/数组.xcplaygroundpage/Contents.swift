//: [Previous](@previous)

import Foundation
//使用［］ 定义数组
//[String] 表示数组是存放字符串到数组
let array = ["zhangsan","lisi"]

//在 Swift 中可以直接将数字放在数组中
// 如果数组中存放的数据类型不一致，自动  推导 的格式是 [NSObject]
let array_2 = ["zhangsan", 19]

// 在日常开发中，类型一致的数组多
// 数组的遍历，是通过下标来访问的，类型不一致，遍历的意义不大



//遍历数组
for name in array{
    print(name)
}


// 可变 var & 不可变 let
var list = ["zhangsan","lisi"]
// 追加元素
list.append("wangwu")
print(list.capacity)

// 删除
list.removeAtIndex(2)//下标从 0 开始
print(list.capacity)

/*
  调试数组容量的方法
  1.定义一个数组
  2.追加元素，跟踪容量变化
  容量 的变化是从1开始，不够的时候，再添加元素，会在当前容量的基础上 ＊2
 
 */
var list_1 = ["有","人","在"]
list_1.append("吗")
print("\(list_1.capacity)")

// 定义一个只能存放字符串的数组,并实例化数组对象

var list_2 = [String]()

for i in 0...16{
    list_2.append("hello \(i)")
    print("索引 \(i) 数组容量\(list_2.capacity)")
}

// 定义一个只能存放整形的数组，且固定容量
var list_3 = [Int]()
for i in 0..<8{
    list_3.append(i)
    print("\(i) 数组容量\(list_3.capacity)")
}
//定义一个只能存放整数的数组,没有进行实例化，就不能放东西，没有实例化 数组对象 是不存在的
var list_4 : [Int]

// 定义一个只能存放整形的数组，且固定容量
// count 是数组的容量 repeatedValue 数组的初始值
var list_5 = [Int](count:32, repeatedValue:3)
print("\(list_5.capacity)")

//数组的拼接
var arr_1 = [1, 2, 3]
var  arr_2 = [4, 5]
var arr_3 = arr_1 + arr_2
//注意，拼接的时候，数组的类型不需一样的



