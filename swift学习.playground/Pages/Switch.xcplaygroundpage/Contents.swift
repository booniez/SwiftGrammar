//: [Previous](@previous)

import Foundation

var str = "Hello, playground"

//: [Next](@next)
//: oc 中 Switch 的特点
/*
   1.表达式必须是一个整数
   2.如果内部定义变量，需要使用 {} 来指明作用域
   3.每一个 case 都需要 break
 */


//在 Swift 中
/*
  1.值可以是任何类型的
  2.作用域 仅仅在 case 内部
  3.不需要 break
  4.每一个 case 都需要有代码
 
 
 */

let name = "张三"
switch name {
    case "袁量":
    print("这是袁量")
case "张亮","张三"://多个条件使用 ， 分隔
    print("这是袁量或者袁量的朋友")
default:
    print("和袁量没有关系")
}