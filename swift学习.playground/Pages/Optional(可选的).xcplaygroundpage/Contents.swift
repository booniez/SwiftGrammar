//: [Previous](@previous)

import Foundation
import UIKit
var str = "Hello, playground"

//: [Next](@next)
//Optional  可以有值，可以为   nil

//init? 可选的，可能无法实例化 url
let Url = NSURL(string: "http://www.baidu.com/中文")

//  ! 强行解包，程序员认为这里   url  一定有值，一旦程序崩溃，就会停在此处
//  错误提示是让程序员思考到底有没有值，代码的安全性会更好
//let request = NSURLRequest(URL: Url!)//不是很安全


//安全的写法
if Url != nil{
    let request = NSURLRequest(url: Url! as URL)
}

//if let 判断并且设置数值
//确保myUrl 一定有值， 才进入分支
if let myUrl = Url {
    print(myUrl)
}
else{
  print("此处的 myUrl 是没有值的...")
}


var oName: String? = "张三"
var oAge: Int? = 18
// 多值之间使用 ， 分隔
if let name = oName,let age = oAge{
    print(name + String(age))
}
// ??操作符号
//如果 oName 为 nil ，使用 ？？ 后面的字符串 ，否则使用 oName 的结果
let cName = oName ?? "abc"

//  ?? 常见的应用场景，表格里要返回数据数量
var dataList: [String]?
dataList = ["张三","李四"]

//dataList? 表示 dataList 可能为 nil
//如果为 nil ， .count 不会报错，因为，像一个空对象可以发送任何消息 ，仍然返回 nil 
//如果不是 nil， .count 会执行并返回数组计数

let count = dataList?.count ?? 0

// !表示程序员承诺 dataList 一定有值，为 nil 就崩
// 每一次写 ！ 强行解包 ，一定要思考！
// 并且要么是强行解包，要么是可选的（  ！  或者   ？）否则，程序编译回报错的

let count1 = dataList!.count

//提示，可选项是所有 oc 程序猿开始最痛苦的一天
//  1.利用 Xcode 的提示
//  2.多思考！
