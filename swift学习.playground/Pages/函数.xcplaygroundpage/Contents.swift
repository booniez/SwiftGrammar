//: [Previous](@previous)

import Foundation

var str = "Hello, playground"

//: [Next](@next)
func sum(x: ino_t ,y: ino_t) -> ino_t {
    return x + y
    
}
sum (x:10, y: 20)
//外部参数 num1， num2 时供外部调用的程序员参考，保证函数的语义更加清晰
// 内部参数 x ， y 数据函数内部使用

func sum1(sum1 x: integer_t, num2 y: integer_t) ->integer_t{
    return x + y
}
sum1(sum1: 3, num2: 2)


//返回值 ->
/* 没有返回值有三种写法，主要是为了闭包
 1.直接省略
 2. -> Viod
 3. -> ()
 
 **/
func demo() {
    print("😄")
}
demo()

func demo1() -> Void{
    print("😄")
}
demo1()

func demo3() -> () {
    print("😄")
}
demo3()
