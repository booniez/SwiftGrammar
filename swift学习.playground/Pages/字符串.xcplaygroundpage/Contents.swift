//: [Previous](@previous)

import Foundation
import UIKit


/**
   String 是一个结构体类型，效率比对象高，推荐使用 String，支持遍历
 

   NSString 是一个对象类型，继承NSObject
 
 **/
var str: String = "你好 ，世界"
//支持遍历
for c in str.characters{
    print(c)
    
}
//: [Next](@next)

//字符串拼接
let name: String? = "袁量"
let age = 20
let object = "Swift"
let rect = CGRect(x: 0, y: 0, width: 12, height: 12)
//print( name + String(age) + object)
print(name ?? "" + String(age) + object)//如果， name 不为 nil 那么将在 name 输出后停止
// \(变量名) 就会自动转化拼接
//

print(  "\(name)  \(age)  \(object) \(rect)")
//如果是可选项的转换，会带上 Optional 提示开发人员，这个是可选的
// 拼接字符串有一个小陷阱 ？
// 如果前面的常量有一个可选的，那么 后面的输出将会报错，因为任何两个值之间计算必须是相同的类型，有一个是可选的还是会报错

//如果真的需要格式
let h = 8
let m = 12
let s = 8
print("\(h) :\(m) :\(s)")

let timeStr = ("\(h) :\(m) :\(s)")

let timeStr_1 = String(format: "%02d:%02d:%02d", arguments: [h , m, s])
//在 Swift 中如果碰到 Range ，最好将 String 改成 NSString
(str as NSString).substring(with: NSMakeRange(1, 4))
//从1 开始，长度为 4

//如果是‘简单的取值’可以这样写
//str.substring(from: "好")
//str.substring(from: 好)










