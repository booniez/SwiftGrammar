//: Playground - noun: a place where people can play

import UIKit


//var str = "Hello, playground"
/*
     1.没有（）
     2.必须有｛ ｝
     3.逻辑分支必须有花括号
     4.没有非零即真，swift中之后 true／false
     5.条件必须指明逻辑结果
 */
let num = 20
if num > 10{
    print("大于 10")
}
/*

Swift中三目用的非常多
 */
let a = 79
let b = 20

let c = a > b ? 100 : -100



//    在 Swift 中，如果不注意， 会让代码写的非常丑陋