//: [Previous](@previous)

import Foundation
import UIKit

protocol Vehicle
{
    var numberOfWheels: Int {get}
    var color: UIColor {get set}
    mutating func changeColor() // 把这里的 mutating 去掉的话， MyCar 是编译不通过的，报错说没有实现的接口
}
struct MyCar: Vehicle {
    let numberOfWheels = 4
    var color = UIColor.blue
    mutating func changeColor() { // 这里的 mutating 去掉的话，会报错说不能改变结构体的成员
        color = UIColor.red
    }
}
//          GeneratorType protocol     
// GeneratorType        typealias Element 
//          Element?     next()

