//: [Previous](@previous)

import Foundation

//定义字典 [String : String]
// [key:value]
// value 可以是 String 活着是 任意类型
// key 通常是字符串
var dic = ["name":"袁量","age":"20","height":"19"]


// 可变  var   &    不可变   let
var dic_1 = ["name":"张良","age":"18"]
dic_1["sex"] = "女"

dic_1
//在设置字典的时候，如果 key 已经存在，就会被覆盖，没有则创建

// k,v 是随便写，前面是 key  ，后面是  value
for (k,v) in dic_1{
    print("key 是\(k)   value 是 \(v)")
    
    
}

for (k,v) in dic{
    dic_1[k] = v
}

dic_1
//: [Next](@next)
